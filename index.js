//learn about modules in node.js

//core modules
const os = require('os') 

//local modules
const rumusSegitiga = require('./rumusSegitiga')
const movies = require("./movies")

//third party modules
// const isOdd = require ("is-ood")


// core modules : module bawaan dari node.js, bisa dilihat di documentation node.js
console.log("free ram is : ", os.freemem())

// local module : module yang kita buat dengan memanfaatkan exports modules
console.log(rumusSegitiga.kelilingSegitiga(10, 5, 2));
console.log(rumusSegitiga.luasSegitiga(10, 5));

// third party module : module yang dibuat bebas oleh orang yang ada di internet
// console.log("is it odd? ", isOdd(4))


//movies

const movie = {
    title : "Bersekutu dengan Iblis",
    genre : [ 
        "mistery",
        "triller",
        "horror"
    ],
    duration : 300,
    studio : "MD Pictures"

}

console.log(process.argv)
if(Number(process.argv[2]) == 1){
    console.log(movies.getListMovies());
}else if (Number(process.argv[2]) == 2){
    movies.getDetailMovie(process.argv[3])
}
