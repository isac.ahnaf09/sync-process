const luasSegitiga = (a, t) => {
    return a * t / 2;
}

const kelilingSegitiga = (a, b, c) => {
    return a + b + c;
}

module.exports = {
    luasSegitiga,
    kelilingSegitiga
} 