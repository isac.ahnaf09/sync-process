const fs = require("fs");
const { title } = require("process");

const getListMovies = () =>{
    const moviesList = fs.readFileSync("./movies.json", "utf-8")

    return JSON.parse(moviesList); 
}

const addMovie = (movie) =>{
    const movies = getListMovies()
    movies.push(movie);

    fs.writeFileSync("./movies.json", JSON.stringify(movies)) 
}

const deleteMovie = (title) =>{
    const movies = getListMovies()

    const moviesFilltered = movies.filter(( ) => {
        return movie.title !== title;
    })

    fs.writeFileSync("./movies.json", JSON.stringify(moviesFilltered));
}

const getDetailMovie = (title) =>{
    const movies = getListMovies();

    movies.forEach(movie => {
        if(movie.title == title){
            console.log(movie);
        }
    });
}

module.exports = {
    getListMovies,
    addMovie,
    deleteMovie,
    getDetailMovie


    // getListMovies : () =>{
    //     const moviesList = fs.readFileSync("./movies.json", "utf-8")

    //     return JSON.parse(moviesList); 
    // },
    // addMovie: (movie) =>{
    //      const movies = getListMovies
    //      movies.push(movie);

    //      return movies;
    // }
}

